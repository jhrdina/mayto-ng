'use strict';

angular.module('maytoApp', [
  'ngAnimate',
  'ngCookies',
  'ngPopover',
  'ngResource',
  'ngSanitize',
  'ui.router',
  'ui.bootstrap',
  'monospaced.mousewheel',
  'angularFileUpload',
  'firebase'
])
  .config(function ($stateProvider, $urlRouterProvider, $locationProvider) {
    $urlRouterProvider
      .otherwise('/');

    $locationProvider.html5Mode(true);
  })
  .constant('FIREBASE_URL', 'https://maytoapp.firebaseio.com/');