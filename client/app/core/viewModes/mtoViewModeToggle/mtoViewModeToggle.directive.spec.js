'use strict';

describe('Directive: mtoViewModeToggle', function () {

  // load the directive's module and view
  beforeEach(module('maytoApp'));
  beforeEach(module('app/core/viewModes/mtoViewModeToggle/mtoViewModeToggle.html'));

  var element, scope;

  beforeEach(inject(function ($rootScope, $compile) {
    scope = $rootScope.$new();
    scope.viewMode = 0;
    scope.vms = [
      {
        title: "AAA",
        buttonFAIcon: "fa-eye"
      },
      {
        title: "BBB",
        buttonFAIcon: "fa-bold"
      }
    ];

    element = angular.element('<mto-view-mode-toggle view-modes="vms" selection="viewMode"></mto-view-mode-toggle>');
    element = $compile(element)(scope);
    scope.$apply();
  }));

  it("active view button should have 'active' class", function() {
    var btns = element.children();

    expect($(btns[0]).hasClass('active')).toBeTruthy();
    expect($(btns[1]).hasClass('active')).toBeFalsy();

    $(btns[1]).click();

    expect($(btns[0]).hasClass('active')).toBeFalsy();
    expect($(btns[1]).hasClass('active')).toBeTruthy();
  });
});