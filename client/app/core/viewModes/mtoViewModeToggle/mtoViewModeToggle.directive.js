'use strict';

angular.module('maytoApp')
  .directive('mtoViewModeToggle', function () {
    return {
      templateUrl: 'app/core/viewModes/mtoViewModeToggle/mtoViewModeToggle.html',
      restrict: 'E',
      replace: true,
      scope: {
        viewModes: '=',
        selection: '='
      },
      controller: function ($scope) {
        $scope.setActiveView = function (index) {
          $scope.selection = index;
        };
      }
    };
  });