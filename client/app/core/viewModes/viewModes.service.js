'use strict';

angular.module('maytoApp')
  .factory('viewModes', function () {
    function Manager() {
    }

    var defaultAttrs = {
      title: null,

      settingsTemplateUrl: null,
      settingsTemplate: null,

      templateUrl: null,
      template: null,

      buttonFAIcon: null,
      buttonCustomIcon: null
    };

    Manager.viewModes = [];

    Manager.addViewMode = function (viewModeAttrs) {
      var attrs;
      attrs = angular.copy(defaultAttrs);
      angular.extend(attrs, viewModeAttrs);

      Manager.viewModes.push(attrs);
    };

    // Public API here
    return Manager;
  });