'use strict';

angular.module('maytoApp')
  .controller('MainCtrl', function($scope, $document, $modal, documentTree, viewModes, User, Node) {
    $scope.layer = 0;
    $scope.viewModes = viewModes.viewModes;
    $scope.activeView = 0;
    $scope.getOpenedNodeId = Node.getOpenedNodeId;

    $scope.openLoginDialog = function() {
      $modal.open({
        templateUrl: 'app/core/auth/authDialog/authDialogLogin.html',
        controller: 'AuthDialogCtrl',
        size: 'sm'
      });
    };

    $scope.openRegisterDialog = function() {
      $modal.open({
        templateUrl: 'app/core/auth/authDialog/authDialogRegister.html',
        controller: 'AuthDialogCtrl',
        size: 'sm'
      });
    };

    $scope.logout = function() {
      User.logout();
    };

    $scope.newFile = function() {
      //documentTree.clear();
    };

    $scope.undo = function() {
      $document[0].execCommand('Undo', false, null);
    };

    $scope.redo = function() {
      $document[0].execCommand('Redo', false, null);
    };

    $scope.setBold = function() {
      $document[0].execCommand('Bold', false, null);
    };

    $scope.setItalic = function() {
      $document[0].execCommand('Italic', false, null);
    };

    $scope.setUnderline = function() {
      $document[0].execCommand('Underline', false, null);
    };

    $scope.openFile = function(fileInputId) {
      $('#' + fileInputId).click();
    };

    $scope.onOpenFileSelect = function($files) {
      var reader = new FileReader();

      reader.onload = function(e) {
        $scope.$apply(function() {
          //documentTree.loadFromJson(e.target.result);
        });
      };

      reader.readAsText($files[0]);
    };

    $scope.openPreferences = function() {
      $modal.open({
        templateUrl: 'app/core/settings/prefsDialog/prefsDialog.html',
        controller: 'PrefsDialogCtrl'
      });
    };

    $scope.openKeyShortcuts = function() {
      $modal.open({
        templateUrl: 'app/core/keyShortcuts/keyShortcuts.html',
        controller: 'KeyShortcutsCtrl'
      });
    };

    $scope.openAbout = function() {
      $modal.open({
        templateUrl: 'app/functionality/about/about.html',
        controller: 'AboutCtrl'
      });
    };
  });