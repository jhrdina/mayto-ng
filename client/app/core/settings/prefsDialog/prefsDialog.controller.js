'use strict';

angular.module('maytoApp')
  .controller('PrefsDialogCtrl', function ($scope, $modalInstance, $modal, messageBox) {
    $scope.close = function () {
      $modalInstance.close();
    };

    $scope.askResetDefault = function () {
      messageBox.open({
        primaryMsg: 'Restore default settings?',
        secondaryMsg: 'Are you sure you want to restore default settings? This action cannot be undone.',
        primaryBtn: 'Restore defaults',
        onOK: undefined,
        onCancel: undefined
      });
    };

    $scope.askEnableDependencies = function () {
      var modalInstance = $modal.open({
        templateUrl: 'app/core/settings/prefsDialog/msgBoxDeps/msgBoxDeps.html',
        controller: 'MsgBoxDepsCtrl',
        resolve: {
          deps: function () {
            return [
              {
                name: 'Images'
              },
              {
                name: 'Source Code'
              }
            ];
          }
        }
      });

      modalInstance.result.then(function (proceed) {
        console.log('Proceed: ' + proceed);
      }, function () {
        console.log('Modal dismissed.');
      });
    };
  });
