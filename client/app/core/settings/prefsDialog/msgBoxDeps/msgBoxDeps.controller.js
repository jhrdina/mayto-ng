'use strict';

angular.module('maytoApp')
  .controller('MsgBoxDepsCtrl', function ($scope, $modalInstance, deps) {
    $scope.deps = deps;

    $scope.ok = function () {
      $modalInstance.close(true);
    };

    $scope.cancel = function () {
      $modalInstance.dismiss('cancel');
    };
  });