'use strict';

angular.module('maytoApp')
  .directive('mtoAuthPanel', function () {
    return {
      templateUrl: 'app/core/auth/mtoAuthPanel/mtoAuthPanel.html',
      restrict: 'E',
      scope: {
        signedIn: '=authSignedIn',
        userName: '=authUserName',
        login: '&authLogin',
        register: '&authRegister',
        userClick: '&authUserClick',
        logout: '&authLogout'
      }
    };
  });