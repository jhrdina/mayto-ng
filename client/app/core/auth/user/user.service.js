'use strict';

angular.module('maytoApp')
  .factory('User', function($firebase, $firebaseSimpleLogin, FIREBASE_URL, $rootScope, $q) {

    // Entity for basic authentication
    var auth = $firebaseSimpleLogin(new Firebase(FIREBASE_URL));

    // Ref with additional user info
    var usersRef = new Firebase(FIREBASE_URL + 'users-metadata');

    function setCurrentUser(uid) {
      $rootScope.currentUser = User.findByUid(uid);
    }

    var User = {
      register: function(user) {
        var defer = $q.defer();

        auth.$createUser(user.email, user.password).then(function(authUser) {

          // Save record with additional info about the registered user
          $firebase(usersRef.child(authUser.uid)).$set({
            name: user.name

          }).then(function() {
            setCurrentUser(authUser.uid);
            defer.resolve($rootScope.currentUser);

          }, function(error) {
            defer.reject(error);
          });
        }, function(error) {
          defer.reject(error);
        });

        return defer.promise;
      },
      login: function(user) {
        return auth.$login('password', user);
      },
      logout: function() {
        auth.$logout();
      },

      findByUid: function(uid) {
        if (uid) {
          return $firebase(usersRef.child(uid)).$asObject();
        }
      },
      getCurrent: function() {
        return $rootScope.currentUser;
      },
      signedIn: function() {
        return $rootScope.currentUser !== undefined && auth.user !== null;
      }
    };

    $rootScope.$on('$firebaseSimpleLogin:login', function(e, authUser) {
      setCurrentUser(authUser.uid);
    });

    $rootScope.$on('$firebaseSimpleLogin:logout', function() {
      delete $rootScope.currentUser;
    });

    $rootScope.signedIn = function() {
      return User.signedIn();
    };

    return User;
  });