'use strict';

angular.module('maytoApp')
  .controller('AuthDialogCtrl', function ($scope, $modalInstance, User) {
    $scope.user = {};

    if (User.signedIn()) {
      $modalInstance.close();
    }

    $scope.register = function () {
      User.register($scope.user).then(function () {
        $modalInstance.close();
      }, function (error) {
        $scope.error = error.toString();
      });
    };

    $scope.login = function () {
      User.login($scope.user).then(function () {
        $modalInstance.close();
      }, function (error) {
        $scope.error = error.toString();
      });
    };

    $scope.cancel = function () {
      $modalInstance.close();
    };
  });
