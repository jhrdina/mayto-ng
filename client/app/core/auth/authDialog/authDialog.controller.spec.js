'use strict';

describe('Controller: AuthDialogCtrl', function () {

  // load the controller's module
  beforeEach(module('maytoApp'));

  var AuthDialogCtrl, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    AuthDialogCtrl = $controller('AuthDialogCtrl', {
      $scope: scope
    });
  }));
});
