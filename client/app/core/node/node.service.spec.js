'use strict';

describe('Service: Node', function () {

  // load the service's module
  /*beforeEach(module('maytoApp'));
  beforeEach(module('mock.firebase'));
  beforeEach(module('firebase'));*/

  beforeEach(function() {
    //module('firebase');
    module('mock.firebase');
    module('maytoApp');
  });
    
  //beforeEach(module('mock.utils'));

  // instantiate service
  var Node, node;

  beforeEach(inject(function (_Node_) {
    Node = _Node_;
    node = new Node('first', null);
  }));

  describe('Constructor', function () {
    it('Parent node should contain child node created with "parent" parameter', function () {
      var myNode = new Node('myChild', node);
      expect(node.children).toContain(myNode);
    });

    it('"text" attribute should be \'\' when instantiated without parameters', function () {
      var myNode = new Node();
      expect(myNode.text).toBe('');
    });
  });

  describe('$parent', function () {
    it('should be null when instantiated without "parent" parameter', function () {
      var myNode = new Node('test');
      expect(myNode.$parent).toBeNull();
    });

    it('should be set when instantiated with "parent" parameter', function () {
      var myNode = new Node('myChild', node);
      expect(myNode.$parent).toBe(node);
    });
  });

  describe('addChild', function () {
    var node2;

    beforeEach(function() {
      node2 = new Node('second', null);
      node.addChild(node2);
    });

    it('should add a new child node', function () {
      expect(node.children).toContain(node2);
    });

    it("should set child's parent to this (if not set)", function() {
      expect(node2.$parent).toBe(node);
    });
  });

  describe('hasChild', function () {
    it('returns false if a node is not there', function () {
      var node2 = new Node('second', null);

      expect(node.hasChild(node2)).toBeFalsy();
    });

    it('returns true if a node is added', function () {
      var node2 = new Node('second', null);
      node.addChild(node2);

      expect(node.hasChild(node2)).toBeTruthy();
    });

    it('returns false for invalid parameter', function () {
      expect(node.hasChild('something')).toBeFalsy();
    });
  });

  describe('hasChildren', function () {
    it('returns false if there are no children', function () {
      expect(node.hasChildren()).toBeFalsy();
    });

    it('returns true if there are one or more children', function () {
      node.addChild(new Node('second', null));
      node.addChild(new Node('third', null));
      expect(node.hasChildren()).toBeTruthy();
    });
  });

  describe('addParam', function () {
    it('should add a new key-value pair', function () {
      node.addParam('myKey', 'myValue');
      expect(node.params.myKey).toBe('myValue');
    });
  });

  describe('isValidNode', function () {
    it('returns true for proper Node instance', function () {
      expect(Node.isValidNode(node)).toBeTruthy();
    });

    it('returns true for generic objects with correct attributes', function () {
      var obj = {
        text: 'some text',
        children: [],
        params: {}
      };
      expect(Node.isValidNode(obj)).toBeTruthy();
    });
  });

  describe('forEachParent', function () {
    var ch1, ch2, ch3;

    beforeEach(inject(function () {
      ch1 = new Node('child1', node);
      ch2 = new Node('child2', ch1);
      ch3 = new Node('child3', ch2);
    }));

    it('number of callback calls should be the same as parents count', function () {
      var counter = 0;
      ch3.forEachParent(function () {
        counter++;
      });
      expect(counter).toBe(3);
    });

    it('parents iteration order should be from the closest to the furthermost', function () {
      var counter = 0;
      ch3.forEachParent(function (parent) {
        counter++;
        switch(counter) {
        case 1:
          expect(parent).toBe(ch2);
          break;
        case 2:
          expect(parent).toBe(ch1);
          break;
        case 3:
          expect(parent).toBe(node);
          break;
        }
      });
    });

    it('iteration should stop if callback returns false', function () {
      var counter = 0;
      ch3.forEachParent(function () {
        counter++;
        if (counter === 2) {
          return false;
        }
      });
      expect(counter).toBe(2);
    });
  });
});
