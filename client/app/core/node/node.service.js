'use strict';

angular.module('maytoApp')
  .factory('Node', function($firebase, FIREBASE_URL, $rootScope, $q) {

    var userRef;
    var rootNodeId;
    var openedNodeId;
    var nodes;

    ///////////////////////////////////////////////////////
    // Private methods

    function init() {
      if (nodes && nodes.$destroy) {
        nodes.$destroy();
      }

      nodes = [];
      rootNodeId = null;
      openedNodeId = null;
      userRef = null;
    }

    function firstLoginInit() {
      // Add first empty node
      return addNode({
        text: ''
      }).then(function(newNodeId) {

        // Set it as rootNode and lastOpenedNode
        return $firebase(userRef).$update({
          rootNodeId: newNodeId,
          lastOpenedNode: newNodeId

        }).then(function() {
          rootNodeId = newNodeId;
          openedNodeId = newNodeId;

          return newNodeId;
        });
      });
    }

    function addNode(node) {
      var defer = $q.defer();
      nodes.$add(node).then(function(ref) {
        defer.resolve(ref.name());
      }, function(error) {
        defer.reject(error);
      });
      return defer.promise;
    }

    ///////////////////////////////////////////////////////
    // Callbacks

    $rootScope.$on('$firebaseSimpleLogin:login', function(e, authUser) {
      Node.loadForUser(authUser.uid);
    });

    $rootScope.$on('$firebaseSimpleLogin:logout', function() {
      init();
    });

    ///////////////////////////////////////////////////////
    // Public members

    var Node = {

      all: nodes,

      loadForUser: function(uid) {

        userRef = new Firebase(FIREBASE_URL + 'users-nodes/' + uid);

        nodes = $firebase(userRef.child('nodes')).$asArray();
        nodes.$loaded().then(function() {

          if (nodes.length === 0) {
            firstLoginInit();
          } else {
            // Get rootNodeId and lastOpenedNode

            userRef.child('rootNodeId').once('value', function(snap) {
              rootNodeId = snap.val();

              userRef.child('lastOpenedNode').once('value', function(snap) {
                openedNodeId = snap.val();
                $rootScope.$apply();
              });
            });
          }
        });

        Node.all = nodes;
      },

      /////////////////////////////////////////////////////
      // Searching

      byId: function(nodeId) {
        return nodes.$getRecord(nodeId);
      },

      idExists: function(nodeId) {
        return Node.byId(nodeId) !== null;
      },

      /////////////////////////////////////////////////////
      // Basic operations

      /**
       * Saves changes in node to the database.
       * @param  {Object} node Changed node object
       * @return {Promise}
       */
      save: function(node) {
        if (!Node.isSynced(node)) {
          throw new Error('You cannot save node that is not in database.');
        }
        return nodes.$save(node);
      },

      /**
       * Deletes node from database and from its parent.
       * @param  {Object} node Node to be deleted. When root node passed,
       *                       operation won't proceed.
       */
      delete: function(node) {
        if (!Node.idExists(node.parentId)) {
          // Cannot delete root node
          return;
        } else {
          Node.delChild(Node.byId(node.parentId), node);
        }
      },

      /////////////////////////////////////////////////////
      // Children

      hasChildren: function(node) {
        return angular.isArray(node.children) && node.children.length > 0;
      },

      hasChild: function(node, childNode) {
        return node.children.indexOf(childNode.$id) !== -1;
      },

      addChild: function(node, childNode) {

        if (!$rootScope.signedIn()) {
          return;
        }

        // TODO: Remove childNode from its old parent

        childNode.parentId = node.$id;

        if (!Node.hasChildren(node)) {
          node.children = [];
        }

        if (!Node.isSynced(childNode)) {
          addNode(childNode).then(function(newId) {
            node.children.push(newId);
            Node.save(node);
          });

          // We're moving child from somewhere else
        } else if (!(childNode.$id in node.children)) {
          // save new child's parentId
          Node.save(childNode);

          node.children.push(childNode.$id);
          Node.save(node);
        }
      },

      delChild: function(node, childNode) {
        nodes.$remove(childNode).then(function() {
          var index = node.children.indexOf(childNode.$id);

          if (index > -1) {
            node.children.splice(index, 1);
          }

          Node.save(node);
        });
      },

      /////////////////////////////////////////////////////
      // Parameters

      addParam: function(node, key, value) {
        if (!node.params) {
          node.params = {};
        }
        node.params[key] = value;
        return Node.save(node);
      },

      hasParam: function(node, key) {
        return node.params && node.params.hasOwnProperty(key);
      },

      delParam: function(node, key) {
        delete node.params[key];
        return Node.save(node);
      },

      /////////////////////////////////////////////////////
      // Backend

      isSynced: function(node) {
        return node.$id !== undefined;
      },

      /////////////////////////////////////////////////////
      // Navigation

      getOpenedNodeId: function() {
        return openedNodeId;
      },

      openNode: function(node) {
        if (node.$id === undefined) {
          return;
        }

        openedNodeId = node.$id;
        userRef.child('lastOpenedNode').set(node.$id);
      },

      /////////////////////////////////////////////////////
      // Multi-node operations

      forEachParent: function(node, callback) {
        var parent = Node.byId(node.parentId);
        while (parent !== null && callback(parent) !== false) {
          parent = Node.byId(parent.parentId);
        }
      },

      /////////////////////////////////////////////////////
      // Data integrity

      isValidNode: function(object) {
        return object.text && object.children && object.params;
      },

      /**
       * Checks node tree integrity and tries to fix problems.
       */
      repair: function() {
        return;
        // Not ready yet.

        var toDel = angular.copy(nodes);
        var isConnected;
        angular.forEach(nodes, function(node) {

          isConnected = false;

          angular.forEach(node.children, function(childId) {
            if (!Node.idExists(childId)) {
              node.children.splice(node.children.indexOf(childId), 1);
            } else {
              isConnected = true;
            }
          });
        });
      }
    };

    init();

    // Public API here
    return Node;
  });