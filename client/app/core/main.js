'use strict';

angular.module('maytoApp')
  .config(function ($stateProvider) {
    $stateProvider
      .state('main', {
        url: '/',
        templateUrl: 'app/core/main.html',
        controller: 'MainCtrl'
      });
  });