'use strict';

angular.module('maytoApp')
  .controller('KeyShortcutsCtrl', function ($scope, $modalInstance) {
    $scope.close = function () {
      $modalInstance.dismiss('cancel');
    };
  });