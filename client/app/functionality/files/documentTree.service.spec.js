'use strict';

describe('Service: documentTree', function () {

  // load the service's module
  beforeEach(module('maytoApp'));
  beforeEach(module('mock.firebase'));

  // instantiate service
  var documentTree, Node, nodeObj, nodeString;

  beforeEach(inject(function (_documentTree_, _Node_) {
    documentTree = _documentTree_;
    Node = _Node_;
    
    nodeObj = {
      'text': 'C:0 L:1',
      'children': [
        {
          'text': 'C:1 L:2',
          'children': [],
          'params': {}
        },
        {
          'text': 'C:1 L:3',
          'children': [
            {
              'text': 'C:2 L:4',
              'children': [],
              'params': {
                'image': 'images/portraits/vrchlicky.jpg'
              }
            },
            {
              'text': 'C:2 L:5',
              'children': [
                {
                  'text': 'C:0 L:6',
                  'children': [],
                  'params': {}
                }
              ],
              'params': {}
            }
          ],
          'params': {
            'collapse': true
          }
        },
        {
          'text': 'C:1 L:7',
          'children': [],
          'params': {
            'image': 'images/portraits/zeyer.jpg'
          }
        }
      ],
      'params': {}
    };

    nodeString = '{'+
      '"text": "C:0 L:1",'+
      '"children": ['+
        '{'+
          '"text": "C:1 L:2",'+
          '"children": [],'+
          '"params": {}'+
        '},'+
        '{'+
          '"text": "C:1 L:3",'+
          '"children": ['+
            '{'+
              '"text": "C:2 L:4",'+
              '"children": [],'+
              '"params": {'+
                '"image": "images/portraits/vrchlicky.jpg"'+
              '}'+
            '},'+
            '{'+
              '"text": "C:2 L:5",'+
              '"children": ['+
                '{'+
                  '"text": "C:0 L:6",'+
                  '"children": [],'+
                  '"params": {}'+
                '}'+
              '],'+
              '"params": {}'+
            '}'+
          '],'+
          '"params": {'+
            '"collapse": true'+
          '}'+
        '},'+
        '{'+
          '"text": "C:1 L:7",'+
          '"children": [],'+
          '"params": {'+
            '"image": "images/portraits/zeyer.jpg"'+
          '}'+
        '}'+
      '],'+
      '"params": {}'+
    '}';
  }));

  it("rootNode should be an empty Node as default", function() {
    expect(documentTree.rootNode instanceof Node).toBeTruthy();
    expect(documentTree.rootNode.text).toBe('');
  });

  describe("clear", function() {
    it("should set rootNode to an empty Node", function() {
      documentTree.rootNode.text = 'rootNode';
      documentTree.rootNode.addChild(new Node('firstChild'));
      documentTree.rootNode.addChild(new Node('secondChild'));

      documentTree.clear();

      expect(documentTree.rootNode instanceof Node).toBeTruthy();
      expect(documentTree.rootNode.text).toBe('');
    });
  });

  describe('loadFromJson', function () {
    it('should convert generic json object to instances of Node class', function () {
      documentTree.loadFromJson(nodeObj);
      expect(documentTree.rootNode.children[1] instanceof Node).toBeTruthy();
      expect(documentTree.rootNode.children[1].addChild).toBeDefined();
    });

    it('should convert JSON string to instance of Node class', function () {
      documentTree.loadFromJson(nodeString);
      expect(documentTree.rootNode.children[1] instanceof Node).toBeTruthy();
      expect(documentTree.rootNode.children[1].addChild).toBeDefined();
    });
  });

});
