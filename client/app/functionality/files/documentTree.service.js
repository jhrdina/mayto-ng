'use strict';

angular.module('maytoApp')
  .factory('documentTree', function (Node) {
    
    function documentTree() {
      this.workDir = '';

      this.clear();
    }

    documentTree.prototype = {
      clear: function () {
        //this.rootNode = new Node();
      },
      
      loadFromJson: function (json) {
        var jsonRootNode;

        jsonRootNode = typeof json === 'string' ? angular.fromJson(json) : json;
        
        this.clear();

        this.rootNode = Node.nodeFromJsonObject(jsonRootNode, null);
      },

      toJson: function () {
        return angular.toJson(this.rootNode, true);
      }
    };

    // Public API here
    return new documentTree();
  });