'use strict';

angular.module('maytoApp')
  .factory('DemoDataFactory', function ($http) {
    return {
      getData: function () {
        return $http.get('assets/json/demoData.json');
      }
    };
  });
