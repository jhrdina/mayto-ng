'use strict';

describe('Directive: mtoLayersSwitch', function () {

  // load the directive's module and view
  beforeEach(module('maytoApp'));
  beforeEach(module('app/functionality/layers/mtoLayersSwitch/mtoLayersSwitch.html'));

  var element, scope, isolScope;

  beforeEach(inject(function ($rootScope, $compile) {
    scope = $rootScope.$new();
    scope.layer = 0;

    element = angular.element('<mto-layers-switch ng-model="layer"></mto-layers-switch>');
    element = $compile(element)(scope);
    scope.$apply();
    isolScope = element.isolateScope();
  }));

  it("clicking should increment layer by one", function() {
    expect(scope.layer).toBe(0);
    element.click();
    expect(scope.layer).toBe(1);
    element.click();
    expect(scope.layer).toBe(2);
  });

  it("clicking on the switch with layer 4 should loop to 0", function() {
    scope.$apply(function() {
      scope.layer = 4;
    });

    element.click();
    expect(scope.layer).toBe(0);
  });

  describe("scrolling up", function() {
    var inc = function() {
      isolScope.$apply(function() {
        isolScope.increment();
      });
    }

    it("should increment layer by 1", function() {
      expect(scope.layer).toBe(0);
      inc();
      expect(scope.layer).toBe(1);
      inc();
      expect(scope.layer).toBe(2);
    });

    it("shouldn't increment layer above 4", function() {
      scope.$apply(function() {
        scope.layer = 4;
      });

      expect(scope.layer).toBe(4);
      inc();
      expect(scope.layer).toBe(4);
      inc();
      inc();
      expect(scope.layer).toBe(4);
    });
  });

  describe("scrolling down", function() {
    var dec = function() {
      isolScope.$apply(function() {
        isolScope.decrement();
      });
    }

    it("should decrement layer by 1", function() {
      scope.$apply(function() {
        scope.layer = 4;
      });

      expect(scope.layer).toBe(4);
      dec();
      expect(scope.layer).toBe(3);
      dec();
      expect(scope.layer).toBe(2);
    });

    it("shouldn't decrement layer under 0", function() {
      expect(scope.layer).toBe(0);
      dec();
      expect(scope.layer).toBe(0);
      dec();
      dec();
      expect(scope.layer).toBe(0);
    });
  });
});