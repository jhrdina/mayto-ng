'use strict';

angular.module('maytoApp')
  .directive('mtoLayersSwitch', function () {
    
    var LAYERS_NO = 5;

    return {
      restrict: 'E',
      replace: true,
      scope: { layer: '=ngModel' },
      templateUrl: 'app/functionality/layers/mtoLayersSwitch/mtoLayersSwitch.html',

      controller: function($scope) {

        $scope.loop = function () {
          $scope.layer = ($scope.layer + 1) % LAYERS_NO;
        };

        $scope.increment = function () {
          if ($scope.layer < LAYERS_NO - 1) {
            $scope.layer += 1;
          }
        };

        $scope.decrement = function () {
          if ($scope.layer > 0) {
            $scope.layer -= 1;
          }
        };

        $scope.myScroll = function (event, delta, deltaX, deltaY) {

          if (deltaY > 0) {
            $scope.increment();
          } else if (deltaY < 0) {
            $scope.decrement();
          }

          event.preventDefault();
        };
      }
    };
  });
