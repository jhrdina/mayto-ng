'use strict';

describe('Directive: mtoBreadcrumb', function() {

  // load the directive's module and view
  beforeEach(module('maytoApp'));
  beforeEach(module('app/functionality/nodesOpening/mtoBreadcrumb/mtoBreadcrumb.html'));

  var element, scope;

  beforeEach(inject(function($rootScope) {
    scope = $rootScope.$new();
  }));
});