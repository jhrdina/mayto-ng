'use strict';

angular.module('maytoApp')
  .directive('mtoBreadcrumb', function(Node) {
    return {
      templateUrl: 'app/functionality/nodesOpening/mtoBreadcrumb/mtoBreadcrumb.html',
      restrict: 'E',
      scope: {
        nodeId: '=activeNodeId'
      },
      link: function(scope, element, attrs) {

        scope.$watch('nodeId', function(newValue) {
          if (newValue === null) {
            return;
          }

          var node = Node.byId(newValue);
          scope.pathArray = [];
          Node.forEachParent(node, function(parent) {
            scope.pathArray.push(parent);
          });
          scope.pathArray.reverse();
        });
      },
      controller: function($scope) {
        $scope.pathArray = [];
        $scope.openNode = function(node) {
          //debugger;
          Node.openNode(node);
        };
      }
    };
  });