'use strict';

describe('Service: NodesOpening', function () {

  // load the service's module
  beforeEach(module('maytoApp'));

  // instantiate service
  var NodesOpening;
  beforeEach(inject(function (_NodesOpening_) {
    NodesOpening = _NodesOpening_;
  }));

  it('should do something', function () {
    expect(!!NodesOpening).toBe(true);
  });

});
