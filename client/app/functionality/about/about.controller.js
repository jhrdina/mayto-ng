'use strict';

angular.module('maytoApp')
  .controller('AboutCtrl', function ($scope, $modalInstance) {
    $scope.close = function () {
      $modalInstance.dismiss('cancel');
    };
  });