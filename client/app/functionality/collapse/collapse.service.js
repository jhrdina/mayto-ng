'use strict';

angular.module('maytoApp')
  .factory('Collapse', function (Node) {

    var KEY = 'collapse';

    function Collapse() {}

    Collapse.isCollapsed = function (node) {
      return Node.hasParam(node, KEY) && node.params[KEY] === true;
    };

    Collapse.setCollapsed = function (node, collapsed) {
      if (collapsed === false) {
        Node.delParam(node, KEY);
      } else {
        Node.addParam(node, KEY, true);
      }
    };

    Collapse.toggle = function (node) {
      Collapse.setCollapsed(node, !Collapse.isCollapsed(node));
    };

    Collapse.isVisible = function (node) {
      var parentCollapsed = false;

      Node.forEachParent(node, function (parent) {
        if (Collapse.isCollapsed(parent)) {
          parentCollapsed = true;
          return false;
        }
      });

      return !parentCollapsed;
    };

    // Public API here
    return Collapse;
  });
