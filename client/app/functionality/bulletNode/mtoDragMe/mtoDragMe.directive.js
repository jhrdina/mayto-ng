'use strict';

angular.module('maytoApp')
  .directive('mtoDragMe', function () {
    return {
      restrict: 'A',
      link: function(scope, element, attrs) {
        element.sortable({ handle: attrs.mtoDragMe });
      }
    };
  });
