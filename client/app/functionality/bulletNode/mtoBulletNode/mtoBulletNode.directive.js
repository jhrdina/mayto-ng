'use strict';

angular.module('maytoApp')
  .directive('mtoBulletNode', function(Collapse, $compile, Node) {
    return {
      restrict: 'E',
      replace: true,
      scope: {
        nodeId: '='
      },
      templateUrl: 'app/functionality/bulletNode/mtoBulletNode/mtoBulletNode.html',
      link: function(scope, element, attrs) {
        scope.$watch('nodeId', function() {
          scope.node = Node.byId(scope.nodeId);
        });
        scope.node = Node.byId(scope.nodeId);

        scope.isHeading = Boolean(attrs.isHeading);

        var childrenTpl = '<div class="children" ng-hide="Collapse.isCollapsed(node)"><mto-bullet-node ng-repeat="childId in node.children" node-id="childId"></mto-bullet-node></div>';
        if (scope.hasChildren()) {
          $compile(childrenTpl)(scope, function(cloned) {
            element.append(cloned);
          });
        }
      },
      controller: function($scope) {
        $scope.Collapse = Collapse;
        $scope.hasChildren = function() {
          return Node.hasChildren($scope.node);
        };
        $scope.hasParam = function(key) {
          return Node.hasParam($scope.node, key);
        };
        $scope.textChanged = function() {
          Node.save($scope.node);
        };
        $scope.openNode = function(node) {
          Node.openNode(node);
        };
      }
    };
  });