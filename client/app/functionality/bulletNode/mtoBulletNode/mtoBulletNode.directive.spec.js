'use strict';

describe('Directive: mtoBulletNode', function () {

  // load the directive's module and view
  beforeEach(module('maytoApp'));
  beforeEach(module('mock.firebase'));
  beforeEach(module('app/functionality/bulletNode/mtoBulletNode/mtoBulletNode.html'));

  var element, scope, mNode;

  beforeEach(inject(function ($rootScope, Node, $compile) {
    scope = $rootScope.$new();
    scope.node = new Node('ABCD');
    mNode = Node

    element = angular.element('<mto-bullet-node node="node"></mto-bullet-node>');
    element = $compile(element)(scope);
    scope.$apply();
  }));

  it("should contain bullet", function() {
    expect(element.find('.bullet').length).toBeGreaterThan(0);
  });

  it('should contain p element with "ABCD" text', function () {
    expect(element.find('p').text()).toBe('ABCD');
  });

  it("bullet should have 'active' class if it has children and is collapsed", function() {
    
    expect(element.find('.bullet').hasClass('active')).toBeFalsy();

    scope.$apply(function() {
      scope.node.addChild(new mNode('CDE'));
      scope.node.addParam('collapse', true);
    });

    expect(element.find('.bullet').hasClass('active')).toBeTruthy();
  });

  it("bullet node should have 'opened-node' class when isHeading attribute specified", inject(function($compile) {
    var myElm = angular.element('<mto-bullet-node node="node" is-heading="true"></mto-bullet-node>');
    myElm = $compile(myElm)(scope);
    scope.$apply();

    expect(myElm.hasClass('opened-node')).toBeTruthy();
  }));
});