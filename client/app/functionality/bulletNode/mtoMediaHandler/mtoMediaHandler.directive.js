'use strict';

angular.module('maytoApp')
  .directive('mtoMediaHandler', function ($document) {
    return {
      templateUrl: 'app/functionality/bulletNode/mtoMediaHandler/mtoMediaHandler.html',
      restrict: 'E',
      replace: true,
      scope: {rightMargin: '='},
      link: function (scope, element) {

        element.on('mousedown', function(event) {
          // Prevent default dragging of selected content
          event.preventDefault();
          scope.$apply(function() {
            scope.rightMargin = $document.width() - event.pageX;
          });
          
          $document.on('mousemove', mousemove);
          $document.on('mouseup', mouseup);
        });

        function mousemove(event) {
          scope.$apply(function() {
            scope.rightMargin = $document.width() - event.pageX;
          });
        }

        function mouseup() {
          $document.off('mousemove', mousemove);
          $document.off('mouseup', mouseup);
        }
      }
    };
  });