'use strict';

describe('Directive: mtoMediaHandler', function () {

  // load the directive's module and view
  beforeEach(module('maytoApp'));
  beforeEach(module('app/functionality/bulletNode/mtoMediaHandler/mtoMediaHandler.html'));

  var element, scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('Changing value of the "right-margin" variable should affect CSS "right" attribute', inject(function ($compile) {

    scope.myMargin = 20;

    element = angular.element('<mto-media-handler right-margin="myMargin"></mto-media-handler>');
    element = $compile(element)(scope);
    scope.$apply();

    expect(element.css('right')).toBe('20px');
    
    scope.myMargin = 50;
    scope.$apply();

    expect(element.css('right')).toBe('50px');
  }));
});