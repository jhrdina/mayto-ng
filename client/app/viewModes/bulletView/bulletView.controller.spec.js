'use strict';

describe('Controller: BulletViewCtrl', function () {

  // load the controller's module
  beforeEach(module('maytoApp'));

  var BulletViewCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    BulletViewCtrl = $controller('BulletViewCtrl', {
      $scope: scope
    });
  }));
});
