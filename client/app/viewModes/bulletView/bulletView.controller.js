'use strict';

angular.module('maytoApp')
  .controller('BulletViewCtrl', function($scope, Node) {
    $scope.mediaWidth = 20;
    $scope.getOpenedNodeId = Node.getOpenedNodeId;
  });