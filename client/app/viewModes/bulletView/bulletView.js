'use strict';

angular.module('maytoApp')
  .run(function (viewModes) {
    viewModes.addViewMode({
      title: 'Bullet view',
      settingsTemplateUrl: null,
      templateUrl: 'app/viewModes/bulletView/bulletView.html',
      buttonFAIcon: 'fa-list-ul'
    });
  });