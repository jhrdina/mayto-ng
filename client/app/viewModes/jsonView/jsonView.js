'use strict';

angular.module('maytoApp')
  .run(function (viewModes) {
    viewModes.addViewMode({
      title: 'JSON view',
      templateUrl: 'app/viewModes/jsonView/jsonView.html',
      buttonFAIcon: 'fa-code'
    });
  });