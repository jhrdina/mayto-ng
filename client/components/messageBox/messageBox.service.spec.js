'use strict';

describe('Service: messageBox', function () {

  // load the service's module
  beforeEach(module('maytoApp'));

  // instantiate service
  var messageBox;
  beforeEach(inject(function (_messageBox_) {
    messageBox = _messageBox_;
  }));

  it('should open new bootstrap modal', inject(function ($modal) {
    spyOn($modal, 'open').andCallThrough();

    messageBox.open();

    expect($modal.open).toHaveBeenCalled();
  }));
});
