'use strict';

angular.module('maytoApp')
  .factory('messageBox', function ($modal) {
    // Public API here
    return {
      open: function (attrs) {
        if (!attrs) {
          attrs = {};
        }

        var modalInstance = $modal.open({
          templateUrl: 'components/messageBox/messageBox.html',
          controller: 'MessageBoxCtrl',
          size: 'sm',
          resolve: {
            strings: function () {
              return {
                primaryMsg: attrs.primaryMsg,
                secondaryMsg: attrs.secondaryMsg,
                primaryBtn: attrs.primaryBtn || 'OK',
                onOK: attrs.onOK,
                onCancel: attrs.onCancel
              };
            }
          }
        });

        modalInstance.result.then(function () {
          if (attrs.onOK) {
            attrs.onOK();
          }
        }, function () {
          if (attrs.onCancel) {
            attrs.onCancel();
          }
        });
      }
    };
  });
