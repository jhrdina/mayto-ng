'use strict';

angular.module('maytoApp')
  .controller('MessageBoxCtrl', function ($scope, $modalInstance, strings) {
    $scope.primaryMsg = strings.primaryMsg;
    $scope.secondaryMsg = strings.secondaryMsg;
    $scope.primaryBtn = strings.primaryBtn;

    $scope.ok = function () {
      $modalInstance.close(true);
    };

    $scope.cancel = function () {
      $modalInstance.dismiss('cancel');
    };
  });