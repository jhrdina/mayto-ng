'use strict';

describe('Directive: contenteditable', function () {

  // load the directive's module
  beforeEach(module('maytoApp'));

  var element,
    scope;

  beforeEach(inject(function ($rootScope, $compile) {
    scope = $rootScope.$new();
    scope.text = 'ABC';

    element = angular.element('<p contenteditable ng-model="text"></p>');
    element = $compile(element)(scope);
    scope.$apply();
  }));

  it("editing view should change the model", function() {
    expect(scope.text).toBe('ABC');

    element.text('DEF');
    element.blur();
    
    expect(scope.text).toBe('DEF');
  });

  it("editing model should change the view", function() {
    expect(element.text()).toBe('ABC');
    
    scope.$apply(function() {
      scope.text = 'DEF';
    });
    
    expect(element.text()).toBe('DEF');
  });
});