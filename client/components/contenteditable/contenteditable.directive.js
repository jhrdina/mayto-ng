'use strict';

angular.module('maytoApp')
  .directive('contenteditable', function() {
    return {
      restrict: 'A',
      require: 'ngModel',
      link: function (scope, elm, attrs, ctrl) {
        // view -> model
        elm.bind('blur', function() {
          scope.$apply(function() {
            ctrl.$setViewValue(elm.html());
            
            if (attrs.ngChange) {
              scope.$eval(attrs.ngChange);
            }
          });
        });

        // model -> view
        ctrl.$render = function() {
          elm.html(ctrl.$viewValue);
        };
      }
    };
  });